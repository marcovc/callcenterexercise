# Call Center Exercise



## Design:

![entities](https://bytebucket.org/marcovc/callcenterexercise/raw/ff12296e78331895cd4ecda4b2c11a2966aadac2/docs/uml/entities.png)

![repos](https://bytebucket.org/marcovc/callcenterexercise/raw/ff12296e78331895cd4ecda4b2c11a2966aadac2/docs/uml/repositories.png)

![flow](https://bytebucket.org/marcovc/callcenterexercise/raw/ff12296e78331895cd4ecda4b2c11a2966aadac2/docs/uml/fullAppFlow.png)

- - -
## Running

#####From console

The app was built with maven and spring boot, with the maven spring boot plugin included in the dependencies, so issuing the following command at the base directory should start the app:

`mvn spring-boot:run`

The app also uses an H2 in memory db and there’s some test data provided that will be migrated automatically at start-up.
The script directory contains a simple script that hits the controller a number of times provided as parameter when executing it.

Example: issuing the following command on a bash console (like gitBash) will perform 30 request on the controller:


`sh curlPostN.sh 30`


#####From IDE

Project Lombok library was used to speed up development and reduce code boilerplate, in order to run the code from the IDE a plugin and Annotations Processor should be enabled to avoid warnings. On IntelliJ this is achievable by downloading IntelliJ’s Lombok plugin from Jetbrains repositories and enabling annotations processor if not already active in *Build, Execution, Deployment > Compiler > Annotations Processor*.

- - -

## Notes about the solution:

The underlying implementation of the executor has only 10 threads, so no more than that quantity of calls can be attended at once. If there’s no employee free at the time of an incoming call, a message will be displayed to let know of the situation. For simplicity this message is currently just a console log, but it could be translated to an specific response that could be handled by the controller. Another option is to throw an exception in that case an handled it with a Controller Advice class at the edge of the app. If more than 10 concurrent calls hit the controller, the remaining ones will linger in the executor’s queue and then discarded.