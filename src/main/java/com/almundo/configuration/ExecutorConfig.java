package com.almundo.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@EnableAsync
@Configuration
public class ExecutorConfig {
  
  @Value("${concurrent.exec.core-pool-size}")
  private Integer corePoolSize;
  
  @Value("${concurrent.exec.max-pool-size}")
  private Integer maxPoolSize;
  
  @Value("${concurrent.exec.queue-size}")
  private Integer queueSize;
  
  @Bean(name = "threadPoolCallsTaskExecutor")
  public TaskExecutor getAsyncExecutor() {
    final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(this.corePoolSize);
    executor.setMaxPoolSize(this.maxPoolSize);
    executor.setQueueCapacity(this.queueSize);
    executor.setThreadNamePrefix("calls-pool-");
    executor.afterPropertiesSet();
    return executor;
  }
}
