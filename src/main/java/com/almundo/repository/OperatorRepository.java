package com.almundo.repository;

import com.almundo.entity.Operator;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface OperatorRepository extends EmployeeBaseRepository<Operator, Long> {
}
