package com.almundo.repository;

import com.almundo.entity.Director;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface DirectorRepository extends EmployeeBaseRepository<Director, Long>{
}
