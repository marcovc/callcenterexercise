package com.almundo.repository;

import com.almundo.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

@NoRepositoryBean
public interface EmployeeBaseRepository<T extends Employee, E extends Serializable> extends JpaRepository<T, E> {
}
