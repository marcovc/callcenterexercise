package com.almundo.repository;

import com.almundo.entity.Supervisor;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface SupervisorRepository extends EmployeeBaseRepository<Supervisor, Long> {
}
