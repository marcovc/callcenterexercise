package com.almundo.controller;

import com.almundo.entity.Call;
import com.almundo.service.Dispatcher;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/calls")
@RequiredArgsConstructor
@Log4j
public class CallController {

  private final Dispatcher dispatcher;

  @PostMapping
  public ResponseEntity<Void> dispatchIncomingCall(@RequestBody final Call call) {
    log.info(" @ receiving incoming call @ ");
    this.dispatcher.dispatchCall(call);
    return new ResponseEntity<>(HttpStatus.ACCEPTED);
  }
}
