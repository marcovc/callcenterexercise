package com.almundo.runnable;

import com.almundo.entity.Call;
import com.almundo.entity.Employee;
import com.almundo.repository.CallRepository;
import com.almundo.service.EmployeeService;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.concurrent.ThreadLocalRandom;

@Component
@Scope("prototype")
@RequiredArgsConstructor
@Builder
@Log4j
public class CallTask implements Runnable {

  private static final int CALL_MIN_DURATION = 5;
  private static final int CALL_MAX_DURATION = 10;

  private final EmployeeService employeeService;
  private final CallRepository callRepository;

  private final Employee employee;
  private final Call incomingCall;

  private void answerCall(final Employee employee, final Call call) {
    log.info(
        "***** Employee "
            + employee.getName()
            + " taking call from number: "
            + call.getPhoneNumber()
            + " *****");

    // Employee takes call
    final int duration = this.randomWait(CALL_MIN_DURATION, CALL_MAX_DURATION);
    log.info("***** Call finished *****");
    call.setCallDuration(String.valueOf(duration).concat("s"));

    this.callRepository.save(call);
    this.employeeService.replenishEmployee(employee);
  }

  @Override
  public void run() {
    this.answerCall(this.employee, this.incomingCall);
  }

  private int randomWait(final int min, final int max) {
    final int random = ThreadLocalRandom.current().nextInt(min, max +1);
    try {
      Thread.sleep(random * 1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return random;
  }
}
