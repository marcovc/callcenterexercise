package com.almundo.service;

import com.almundo.entity.Call;
import com.almundo.entity.Employee;
import com.almundo.repository.CallRepository;
import com.almundo.runnable.CallTask;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log4j
public class Dispatcher {

  private static final String DEFAULT_EMPLOYEE = "Default";

  private final TaskExecutor executor;

  private final EmployeeService employeeService;

  private final CallRepository callRepository;

  public void dispatchCall(final Call call) {
    log.info(" ^^^^ Incoming call from: " + call.getPhoneNumber() + " ^^^^ ");

    final Employee assignedEmployee = this.employeeService.getAvailableEmployee();

    if (!DEFAULT_EMPLOYEE.equals(assignedEmployee.getName())) {
      final CallTask callTask =
          CallTask.builder()
              .employeeService(this.employeeService)
              .employee(assignedEmployee)
              .incomingCall(call)
              .callRepository(this.callRepository)
              .build();
      this.executor.execute(callTask);
    } else {
      log.info(" ^^^^ No Available Employees can take the call at the moment ^^^^ ");
    }
  }
}
