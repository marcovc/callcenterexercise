package com.almundo.service;

import com.almundo.entity.Director;
import com.almundo.entity.Employee;
import com.almundo.entity.Operator;
import com.almundo.entity.Supervisor;
import com.almundo.repository.DirectorRepository;
import com.almundo.repository.OperatorRepository;
import com.almundo.repository.SupervisorRepository;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Sets;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class EmployeeService {

  private final OperatorRepository operatorRepository;
  private final SupervisorRepository supervisorRepository;
  private final DirectorRepository directorRepository;

  private Set<Employee> operators;
  private Set<Employee> supervisors;
  private Set<Employee> directors;

  private Employee defaultEmployee;

  @PostConstruct
  @VisibleForTesting
  void initializeService() {
    this.defaultEmployee = new Operator();
    this.defaultEmployee.setName("Default");
    this.defaultEmployee.setLastName("Default");

    this.operators = Sets.newConcurrentHashSet(this.operatorRepository.findAll());
    this.supervisors = Sets.newConcurrentHashSet(this.supervisorRepository.findAll());
    this.directors = Sets.newConcurrentHashSet(this.directorRepository.findAll());
  }

  public Employee getAvailableEmployee() {

    final Employee availableEmployee =
        this.operators
            .stream()
            .findFirst()
            .orElse(
                this.supervisors
                    .stream()
                    .findFirst()
                    .orElse(this.directors.stream().findFirst().orElse(this.defaultEmployee)));

    if (availableEmployee.equals(this.defaultEmployee)) {
      return this.defaultEmployee;
    }
    this.occupyEmployee(availableEmployee);
    return availableEmployee;
  }

  private boolean isSupervisor(final Employee employee) {
    return employee.getClass() == Supervisor.class;
  }

  private boolean isDirector(final Employee employee) {
    return employee.getClass() == Director.class;
  }

  private boolean isOperator(final Employee employee) {
    return employee.getClass() == Operator.class;
  }

  private void occupyEmployee(final Employee employee) {
    if (this.isOperator(employee)) {
      this.operators.remove(employee);
    }
    if (this.isSupervisor(employee)) {
      this.supervisors.remove(employee);
    }
    if (this.isDirector(employee)) {
      this.directors.remove(employee);
    }
  }

  public void replenishEmployee(final Employee employee) {
    if (this.isOperator(employee)) {
      this.operators.add(employee);
    }
    if (this.isSupervisor(employee)) {
      this.supervisors.add(employee);
    }
    if (this.isDirector(employee)) {
      this.directors.add(employee);
    }
  }
}
