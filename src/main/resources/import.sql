INSERT INTO director (name, age, address) VALUES ('First director', 33, 'dir');

INSERT INTO supervisor (name, age, address) VALUES ('First supervisor', 29, 'dir');
INSERT INTO supervisor (name, age, address) VALUES ('Second supervisor', 30, 'dir');
INSERT INTO supervisor (name, age, address) VALUES ('Third supervisor', 31, 'dir');
INSERT INTO supervisor (name, age, address) VALUES ('Fourth supervisor', 32, 'dir');

INSERT INTO operator (name, age, address) VALUES ('First operator', 25, 'dir');
INSERT INTO operator (name, age, address) VALUES ('Second operator', 26, 'dir');
INSERT INTO operator (name, age, address) VALUES ('Third operator', 27, 'dir');
INSERT INTO operator (name, age, address) VALUES ('Fourth operator', 28, 'dir');
INSERT INTO operator (name, age, address) VALUES ('Fifth operator', 24, 'dir');