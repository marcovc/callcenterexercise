package com.almundo.service;

import com.almundo.entity.Call;
import com.almundo.entity.Employee;
import com.almundo.entity.Operator;
import com.almundo.repository.CallRepository;
import com.anarsoft.vmlens.concurrent.junit.ConcurrentTestRunner;
import com.anarsoft.vmlens.concurrent.junit.ThreadCount;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.task.TaskExecutor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(ConcurrentTestRunner.class)
public class DispatcherTest {

  private static final int MAX_NUMBER_OF_THREADS = 10;
  private static final String TEST_PHONE_NUMBER = "Test phone number";
  private static final String TEST_AREA_CODE = "Test area code";

  private Call call;

  @Mock private TaskExecutor executor;

  @Mock private EmployeeService employeeService;

  @Mock private CallRepository callRepository;

  private Dispatcher dispatcher;

  private Employee availableEmployee;

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);

    this.dispatcher = new Dispatcher(this.executor, this.employeeService, this.callRepository);
    this.call = new Call(TEST_PHONE_NUMBER, TEST_AREA_CODE);
    this.availableEmployee = new Operator();

    when(this.employeeService.getAvailableEmployee()).thenReturn(this.availableEmployee);
    doNothing().when(this.executor).execute(any(Runnable.class));
  }

  @Test
  @ThreadCount(MAX_NUMBER_OF_THREADS)
  public void shouldDispatchMaxConcurrentCalls() {
    this.dispatcher.dispatchCall(this.call);
  }

  @After
  public void maxConcurrentCallsTestVerification() {
    verify(this.executor, times(MAX_NUMBER_OF_THREADS)).execute(any(Runnable.class));
  }
}
