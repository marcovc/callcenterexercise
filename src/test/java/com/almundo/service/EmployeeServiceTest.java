package com.almundo.service;

import com.almundo.entity.Director;
import com.almundo.entity.Employee;
import com.almundo.entity.Operator;
import com.almundo.entity.Supervisor;
import com.almundo.repository.DirectorRepository;
import com.almundo.repository.OperatorRepository;
import com.almundo.repository.SupervisorRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EmployeeServiceTest {

  private static final String DEFAULT_EMPLOYEE_NAME = "Default";

  @Mock private OperatorRepository operatorRepository;
  @Mock private SupervisorRepository supervisorRepository;
  @Mock private DirectorRepository directorRepository;

  private EmployeeService employeeService;

  private final Operator operator = new Operator();
  private final Supervisor supervisor = new Supervisor();
  private final Director director = new Director();

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);

    this.employeeService =
        new EmployeeService(
            this.operatorRepository, this.supervisorRepository, this.directorRepository);
  }

  @Test
  public void shouldGetOperator() {
    when(this.operatorRepository.findAll()).thenReturn(Collections.singletonList(this.operator));

    this.employeeService.initializeService();
    final Employee actualOperator = this.employeeService.getAvailableEmployee();

    assertThat(actualOperator, instanceOf(Operator.class));
    verify(this.operatorRepository, times(1)).findAll();
  }

  @Test
  public void shouldGetSupervisor() {
    when(this.operatorRepository.findAll()).thenReturn(Collections.emptyList());
    when(this.supervisorRepository.findAll())
        .thenReturn(Collections.singletonList(this.supervisor));

    this.employeeService.initializeService();
    final Employee actualSupervisor = this.employeeService.getAvailableEmployee();

    assertThat(actualSupervisor, instanceOf(Supervisor.class));
    verify(this.operatorRepository, times(1)).findAll();
    verify(this.supervisorRepository, times(1)).findAll();
  }

  @Test
  public void shouldGetDirector() {
    when(this.operatorRepository.findAll()).thenReturn(Collections.emptyList());
    when(this.supervisorRepository.findAll()).thenReturn(Collections.emptyList());
    when(this.directorRepository.findAll()).thenReturn(Collections.singletonList(this.director));

    this.employeeService.initializeService();
    final Employee actualDirector = this.employeeService.getAvailableEmployee();

    assertThat(actualDirector, instanceOf(Director.class));
    verify(this.operatorRepository, times(1)).findAll();
    verify(this.supervisorRepository, times(1)).findAll();
    verify(this.directorRepository, times(1)).findAll();
  }

  @Test
  public void shouldGetDefaultEmployee() {
    when(this.operatorRepository.findAll()).thenReturn(Collections.emptyList());
    when(this.supervisorRepository.findAll()).thenReturn(Collections.emptyList());
    when(this.directorRepository.findAll()).thenReturn(Collections.emptyList());

    this.employeeService.initializeService();
    final Employee actualEmployee = this.employeeService.getAvailableEmployee();

    assertThat(actualEmployee, instanceOf(Operator.class));
    assertEquals(DEFAULT_EMPLOYEE_NAME, actualEmployee.getName());
    verify(this.operatorRepository, times(1)).findAll();
    verify(this.supervisorRepository, times(1)).findAll();
    verify(this.directorRepository, times(1)).findAll();
  }
}
